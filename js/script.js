document.addEventListener('DOMContentLoaded', () => {
    tabsFunc();
    amazingWorkFunc();
});


//////////////////////////  Our services tabs //////////////////////////

const tabsContainer = document.querySelector('.title-tabs')
let tabList = document.querySelectorAll('.title-tabs-items')
let tabItems = document.querySelectorAll('.tab-items')

    const tabsFunc = () => {
        tabsContainer.addEventListener('click', event => {
            let target = event.target;

            tabList.forEach((element) => {
                element.classList.remove('tabs-active')
            })

            target.classList.add('tabs-active');

            tabItems.forEach((el) => {
                if (el.dataset.name === target.dataset.name) {
                    el.classList.add('tab-items-active')
                } else {
                    el.classList.remove('tab-items-active')
                }
            })
        })
    }

//////////////////////////  Our amazing work section //////////////////////////

const amazingTabsContainer = document.querySelector('.amazing-tabs');
let amazingTabList = document.querySelectorAll('.amazing-title-tabs');
let loadBtn = document.querySelector('.load-more-btn');
let imgCont = document.querySelectorAll('.img-container');
let dataAll = document.querySelector('[data-category="All"]')

  const amazingWorkFunc = () => {  amazingTabsContainer.addEventListener('click', evt => {
        let target = evt.target

        amazingTabList.forEach((element) => {
            element.classList.remove('amazing-tabs-active');
        })
        target.classList.add('amazing-tabs-active');

        imgCont.forEach( (el) => {
            if (el.dataset.category === target.dataset.category) {
                el.classList.remove('img-container-hidden');
            } else if (el.dataset.category !== target.dataset.category) {
                el.classList.add('img-container-hidden');
            }
            else {
                el.classList.add('img-container-hidden');
            }

        })
        if (target === dataAll) {
            loadBtn.removeAttribute('id')
        } else {
            loadBtn.id = 'load-btn-hidden'
        }
    })


    loadBtn.addEventListener('click', event => {
        let target = event.target;

        imgCont.forEach((element) => {
            element.classList.remove('img-container-additional')

            if (imgCont.length >= 24) {
                target.remove();
            }
        })
    })
}


/////////////////////// Slider /////////////////////////////

let galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 1,
    width: 400,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
});
let galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 1,
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs,
    },
});


